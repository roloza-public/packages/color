# Color

Trouve la couleur associée à un code hexadécimal

## Utilisation

```php
$color = new \Roloza\Color\Color();
$colors->search('#FFFFFF');
```
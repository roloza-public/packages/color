<?php

namespace Roloza\Color;

class Color
{
    private $colors;

    public function __construct()
    {
        $this->setColors();
    }

    /**
     * @return array
     */
    public function getColors(): array
    {
        return $this->colors;
    }

    /**
     * @param array $colors
     */
    public function setColors($colors = []): void
    {
        if (empty($colors)) {
            $colors = Utils::jsonFileToArray('colors.json');
        }
        $this->colors = $colors;
    }

    /**
     * Get the best colorName of an hexa code
     * @param String $color hexa code
     * @return Array [hex, color, colorName, exact]
     */
    public function search($color) {

        $colorDigit = str_replace('#', '', $color);
        if (strlen($colorDigit) === 3) {
            $color = $color . $colorDigit;
        }

        $color = strtoupper($color);
        $rgb = Utils::rgb($color);
        $hsl = Utils::hsl($color);

        if (!ctype_xdigit($colorDigit) || strlen($color) !== 7) {
            throw new Exception("Invalid Color: " . $color);
        }

        $key = -1;
        $df = -1;

        foreach ($this->colors as $k => $name) {
            if ($color == "#" . $name['code']) {
                return [
                    "hex"       => "#" . $name['code'],
                    "color"     => $name['name'],
                    "colorName" => $name['fullname'],
                    "exact"     => true
                ];
            }

            $nameRgb = Utils::rgb("#" . $name['code']);
            $nameHsl = Utils::hsl("#" . $name['code']);
            $ndf1 =
                pow($rgb['r'] - $nameRgb['r'], 2) +
                pow($rgb['g'] - $nameRgb['g'] , 2) +
                pow($rgb['b'] - $nameRgb['b'], 2);
            $ndf2 =
                pow($hsl['h'] - $nameHsl['h'], 2) +
                pow($hsl['s'] - $nameHsl['s'] , 2) +
                pow($hsl['l'] - $nameHsl['l'], 2);
            $ndf = $ndf1 + $ndf2;
            if($df < 0 || $df > $ndf) {
                $df = $ndf;
                $key = $k;
            }


        }
        if ($key < 0) {
            throw new Exception("Invalid Color: " . $color);
        }

        return [
            "hex"       => "#" . $this->colors[$key]['code'],
            "color"     => $this->colors[$key]['name'],
            "colorName" => $this->colors[$key]['fullname'],
            "exact"     => false
        ];

    }

}
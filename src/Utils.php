<?php
namespace Roloza\Color;

class Utils
{

    /**
     * Transform json file to php array
     */
    public static function jsonFileToArray($file): array
    {
        $json = file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . $file);
        return json_decode($json, true);
    }

    /**
     * Transform hexa code to rgb
     * @param  String $color 'hexa' code of color
     * @return array (r, g, b)
     */
    public static function rgb(String $color): array
    {
        return [
            'r' => hexdec(substr($color, 1, 2)),
            'g' => hexdec(substr($color, 3, 2)),
            'b' => hexdec(substr($color, 5, 7))
        ];
    }

    /**
     * Transform hexa code to hsl
     * @param  String $color 'hexa' code of color
     * @return array (h, s, l)
     */
    public static function hsl(String $color): array {
        $rgb = [
            intval(hexdec(substr($color, 1, 2))) / 255,
            intval(hexdec(substr($color, 3, 2))) / 255,
            intval(hexdec(substr($color, 5, 7))) / 255,
        ];

        $r = $rgb[0];
        $g = $rgb[1];
        $b = $rgb[2];

        $min = min($r, min($g, $b));
        $max = max($r, max($g, $b));

        $delta = $max - $min;
        $l = ($min + $max) / 2;
        $s = 0;
        if ($l > 0 && $l < 1) {
            $s = $delta / ($l < 0.5 ? (2 * $l) : (2 - 2 * $l));
        }
        $h = 0;
        if ($delta > 0) {
            if ($max === $r && $max !== $g) {
                $h += ($g - $b) / $delta;
            }
            if ($max === $g && $max !== $b) {
                $h += (2 + ($b - $r) / $delta);
            }
            if ($max === $b && $max !== $r) {
                $h += (4 + ($r - $g) / $delta);
            }
            $h /= 6;
        }
        return [
            'h' =>intval($h * 255),
            's' =>intval($s * 255),
            'l' =>intval($l * 255),
        ];
    }

}